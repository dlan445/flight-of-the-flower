# compiler
CC = g++

# directories
DIR = src/
DIR-OBJ = $(DIR)obj/
DIR-BUILD = $(DIR)build/
DIR-SFML = lib/SFML/
DIR-SFML-INCLUDE = $(DIR-SFML)include/ # contains .hpp files
DIR-SFML-LIB = $(DIR-SFML)lib/

# source files
CPP = $(DIR)hello.cpp 

# object files
OBJ = hello.o 

# libraries
SFML-SYS = -lsfml-system
SFML-WIN = -lsfml-window $(SFML-SYS)
SFML-GRA = -lsfml-graphics $(SFML-WIN)


#---------------------------------------

# create exe from linking object files
hello: $(OBJ)
	$(CC) -o hello $(OBJ) -L $(DIR-SFML-LIB) $(SFML-GRA)
	make move

# create object files from cpp files
%.o:
	$(CC) -c -g $(CPP) -I $(DIR-SFML-INCLUDE)

# created files are in root directory. This moves them after compilation.
move:
	mv *.o $(DIR-OBJ)
	mv hello.exe $(DIR-BUILD)

# remove exe and object files
clean:
	rm -f $(DIR-BUILD)*.exe
	rm -f $(DIR-OBJ)*.o
	rm -f *.o