# **Flight of the Flower Repository**
Open-source repository for the 'Flight of the Flower' game. Made using C++ and the [SFML library](https://www.sfml-dev.org/index.php).

## Releases
Stable releases are located in `releases\` . See the README within the folder.

The latest executable (Windows) is located in `src\build` of the relevant branch.

## Roadmap  

**alpha**
- a  
- b  
- c  

**beta**
- d  
- e  
- f  

**beta pre-release**
- g  
- h  
- i  

*Release!*



## Compilation instructions
Run `make` from the root directory.

## Branch system
|`branch`|Purpose|
|--------|-------|
|`master`|      Release branch. Tagged with significant (`x.` or `x.x`) releases.|
|`dev-x.x`|         Development branch of version `x.x`. Gathers features from lower branches, ensures their compatibility, pushes to `master`.|
|`f-feature`|   Feature branches. Code development is completed in these branches, and pushed to the `dev` version branch upon completion. No version number is stated as these branches are continually managed.|

## Active branches
|`branch`|Version|Purpose|
|--------|-------|-------|
|`master`|          `0.alpha.00` | Release branch.|
|`dev-0.alpha.01`|  `0.alpha.01` | Current development branch.|
|`f-backend`|       `0.alpha.01` | For setting underlying program structure.|

