\section{Compilation}

    \subsection{g++}

        To compile a \code{c++} program using \code{g++}, you first create the \code{.o} object files, and then link them together into a \code{.exe}.

        To create the \code{.o} file, use the \code{-c} flag. If this isn't included, \code{g++} will attempt to make an \code{.exe} straight away.

        \begin{lstlisting}[style=terminal]
    g++ -c file.cpp
        \end{lstlisting}
        You can designate the output with \code{-o} as
        \begin{lstlisting}[style=terminal]
    g++ -c file.cpp -o file.o
        \end{lstlisting}
        You can specify the directory as a path before the filename, as
        \begin{lstlisting}[style=terminal]
    g++ -c mydir/file.cpp -o mydir/file.o
        \end{lstlisting}
        which can be useful if you want the compiled files to end up in a different directory.

        If we want our executable to be able to be debugged, add the \code{-g} flag:
        \begin{lstlisting}[style=terminal]
    g++ -g -c mydir/file.cpp -o mydir/file.o
        \end{lstlisting}

        To link the \code{.o} files together, we re-run \code{g++}:
        \begin{lstlisting}[style=terminal]
    g++ mydir/file.o -o mydir/file
        \end{lstlisting}
        which creates an executable.

        The following flags in \code{g++} will be used throughout this project.

        \begin{tabular}{p{2cm}p{8cm}}
            \code{-I} & Path for header files outside of code directory \\
            \code{-L} & Path for library files \\
            \code{-l} & Specific library files to link \\
        \end{tabular}

    \subsection{make files}

        Makefiles have the following form:
        \begin{lstlisting}[style=makefile, morekeywords={recipe, variable}]
    variable = definition     # optional
    
    recipe: prerequisites
        command

    ...
        \end{lstlisting}
        We can adapt the following code above into a simple makefile, like so:
        \begin{lstlisting}[style=makefile, morekeywords={file}]
    file: file.o
        g++ -c mydir/file.o -o mydir/file

    file.o:
        g++ -g -c mydir/file.cpp -o mydir/file.o
        \end{lstlisting}
        which we can reduce further by introducing variables, like so:
        \begin{lstlisting}[style=makefile, morekeywords={file, DIR}]
    CC = g++
    DIR = mydir/

    file: file.o
        $(CC) -c $(DIR)file.o -o $(DIR)file

    file.o:
        $(CC) -g -c $(DIR)file.cpp -o $(DIR)file.o
        \end{lstlisting}

    \subsection{Compiling with SFML}

        The SFML files are contained within three subfolders:
        
        \begin{tabular}{p{5cm}p{8cm}}
            \code{SFML\textbackslash bin} & contains \code{.dll} files \\
            \code{SFML\textbackslash include\textbackslash SFML\textbackslash} & contains \code{.hpp} files \\
            \code{SFML\textbackslash lib} & contains \code{.a} files \\
        \end{tabular}

        In my project, I have the parent folder \code{SFML\textbackslash} separate to my source files - it is not heirarchial. I also have my makefile in the root of my repository, so it can access all libraries and source code.

        There are four steps to complete such that your code can use SFML commands.
        \begin{enumerate}
            \item Introduce the appropriate header files in your code. If the SFML library is in a seaparate location (i.e. not a subdirectory) to your source files, then use the \code{\#include <...>} syntax as opposed to the \code{\#include "..."} syntax. 
            
            The SFML documentation includes the inner \code{SFML} directory as part of the header files; as such, include your necessary header files as
            \begin{lstlisting}[style=cpp-code]
    #include <SFML/file.cpp>
            \end{lstlisting}
            We will specify where the \code{SFML} directory is to the compiler, however errors will occur in your IDE/development window as the IntelliSense engine will not have the path to said folder. In VSCode, this is achieved by\footnote{\url{https://code.visualstudio.com/docs/languages/cpp\#_intellisense}}:
            \begin{enumerate}
                \item Click the yellow lightbulb next to the error over the \code{\#include} statement, select `Edit ``includePath'' setting'.
                \item Open the relevant \code{.json} file - a link is at the bottom. This will create and open a \code{c\_cpp\_properties.json} file in your \code{.vscode\textbackslash} directory in your workspace.
                \item Under the \code{``includePath''} setting, add the path to the folder relative to the workspace, e.g.
                \begin{lstlisting}[style=json]
    "${workspaceFolder}/lib/SFML/include",
                \end{lstlisting}
            \end{enumerate}

            \item Tell the compiler when it is creating the object files where the header files are. In your makefile, add the line
            \begin{lstlisting}[style=makefile]
                -I lib/SFML/include/
            \end{lstlisting}
            in the \code{g++} command.

            \item Tell the compiler when it is creating the executable where the SFML libraries are located, and what libraries to use. The \code{-L} flag is the library location, and the \code{-l} flag is specifying what library to use. Both of these commands should be placed \emph{after} the source files. The libraries to use and what order is well-documented on the SFML website - see here\footnote{\url{https://www.sfml-dev.org/tutorials/1.6/start-linux.php}}. As an example:
            \begin{lstlisting}[style=makefile]
                g++ file.o -o file -L lib/SFML/lib/ -lsfml-system
            \end{lstlisting}

            \item Your code will now compile but it will not run. For it to run, the \code{.dll} files in \code{SFML\textbackslash bin} need to be copied into the directory of the executable - this can be done after the compilation process, as they are only required at runtime. After doing so, the exe can be run inside or outside of the command line.
        \end{enumerate}

        A full example (based off of the documentation on the SFML website)\footnote{\url{https://www.sfml-dev.org/tutorials/2.5/start-linux.php} and \url{https://www.sfml-dev.org/tutorials/1.6/start-linux.php}}:

        \textbf{File structure:}

        \texttt{\textbackslash} \\
        \texttt{\tab\tab lib\textbackslash} \\
        \texttt{\tab\tab\tab SFML\textbackslash} \\
        \texttt{\tab\tab\tab\tab bin\textbackslash} \textcolor{gray}{contains the \texttt{.dll} files} \\
        \texttt{\tab\tab\tab\tab include\textbackslash SFML\textbackslash} \textcolor{gray}{contains the \texttt{.hpp} files} \\
        \texttt{\tab\tab\tab\tab lib\textbackslash} \textcolor{gray}{contains the \texttt{.a} files} \\
        \texttt{\tab\tab src\textbackslash} \\
        \texttt{\tab\tab\tab file.cpp} \\
        \texttt{\tab\tab\tab \textcolor{gray}{file.o}} \\
        \texttt{\tab\tab\tab \textcolor{gray}{file.exe}} \\
        \texttt{\tab\tab\tab <.dll files>} \textcolor{gray}{(move here from \texttt{bin\textbackslash} directory)} \\

        \textbf{file.cpp}
        \begin{lstlisting}[style=cpp-code]
    #include <iostream>
    #include <SFML/Graphics.hpp>
    using namespace std;
    
    int main() {
    
        sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
        sf::CircleShape shape(100.f);
        shape.setFillColor(sf::Color::Green);
    
        while (window.isOpen())
        {
            sf::Event event;
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    window.close();
            }
    
            window.clear();
            window.draw(shape);
            window.display();
        }
    
        cout << "Hello world";
    
        return 0;
    }
        \end{lstlisting}

        \textbf{makefile}
        \begin{lstlisting}[style=makefile]
    CC = g++
    DIR = src/
    SFML = lib/SFML/
    SFML-INCLUDE = $(SFML)include/ # contains .hpp files
    SFML-LIB = $(SFML)lib/
    
    file: file.o
        $(CC) $(DIR)file.o -o $(DIR)file -L $(SFML-LIB) -lsfml-graphics -lsfml-window -lsfml-system
    
    file.o:
        $(CC) -g -c $(DIR)file.cpp -o $(DIR)file.o -I $(SFML-INCLUDE)
        \end{lstlisting}

    From the root directory, in terminal, type \code{make}.
